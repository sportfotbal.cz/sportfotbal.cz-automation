#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Import EANs from csv files.

Import EANs from csv files and run each column through filter by header column definition
"""

import os
import pymysql
import sys
import configparser
import neon

from pyparsing import Word, alphas, OneOrMore, Suppress, ZeroOrMore, LineStart, LineEnd, oneOf, Optional, alphanums, Keyword, Group, Empty, NoMatch,  Regex, nums, ParseException, nestedExpr, QuotedString

CONFIG = configparser.ConfigParser()
CONFIG.read(os.path.join(os.path.dirname(__file__),'config.ini'))

config = None

with open(os.path.join(os.path.dirname(__file__),'..','html','neon','config.neon'), 'r') as fd:
    config = neon.decode(fd.read())

class Db:

    def __init__(self):
        self.connection = pymysql.connect(host=CONFIG['ean']['host'],
                                     user=CONFIG['ean']['user'],
                                     password=CONFIG['ean']['password'],
                                     db=CONFIG['ean']['db'],
                                     charset=CONFIG['ean']['charset'],
                                     cursorclass=pymysql.cursors.DictCursor)

    def query(self, query, data=()):
        with self.connection.cursor() as cursor:
            cursor.execute(query, data)
            return cursor

    def putEan(self, id, ean):
        cursor = self.query('''UPDATE product SET ean = %s WHERE id = %s''', (ean,id))
        self.connection.commit()
        return cursor

    def putPrice(self, id, price_cz = None, price_sk = None):

        if price_cz and price_sk:
            price_cz = price_cz/(float(config['www.sportfotbal.cz']['vat'])+1.0)
            price_sk = (price_sk*float(config['www.sportfutbal.sk']['exchange_rate']))/(float(config['www.sportfutbal.sk']['vat'])+1.0)
            pass
        elif price_cz:
            price_sk = price_cz = price_cz/(float(config['www.sportfotbal.cz']['vat'])+1.0)
            pass
        elif price_sk:
            price_cz = price_sk = (price_sk*float(config['www.sportfutbal.sk']['exchange_rate']))/(float(config['www.sportfutbal.sk']['vat'])+1.0)
            pass
        else:
            pass

        for c in config:
            id_currency = config[c]['id_currency']
            id_domain = config[c]['id']

            if id_currency == 1:
                cursor = self.query('UPDATE price SET value = %s WHERE id_product = %s AND id_domain = %s', (price_cz,id,id_domain))
                print((price_cz,id,id_domain))
                #self.connection.commit()
            elif id_currency == 2:
                cursor = self.query('UPDATE price SET value = %s WHERE id_product = %s AND id_domain = %s', (price_sk,id,id_domain))
                print((price_sk,id,id_domain))
                #self.connection.commit()

        return None




    def getByGeneratedEan(self,manufacturer_code,color_code,size_code):
        regexp = f"^{manufacturer_code}:{color_code}:(({size_code})|(.*:{size_code}))((:.*$)|($))"
        cursor = self.query('''SELECT id, ean, generated_ean FROM product WHERE (user_code = %s OR user_code LIKE %s) AND generated_ean REGEXP %s''', (manufacturer_code,manufacturer_code + "-%",regexp))
        return cursor

    def getByEan(self,ean = None, manufacturer_code = None):
        cursor = []

        if ean:
            cursor = self.query('SELECT product.id, product.ean, product.generated_ean FROM product WHERE ean = %s', (ean))
        elif manufacturer_code:
            cursor = self.query('SELECT product.id, product.ean, product.generated_ean FROM product WHERE generated_ean LIKE %s', (f'{manufacturer_code}:%'))

        return cursor

class Filters:

    _us2uk = {
        '9.5C' : '9K',
        '10C' : '9.5K',
        '10.5C' : '10K',
        '11C' : '10.5K',
        '11.5C' : '11K',
        '12C' : '11.5K',
        '12.5C' : '12K',
        '13C' : '12.5K',
        '13.5C' : '13K',
        '1Y' : '13.5C',
        '1.5Y' : '1Y',
        '2Y' : '1.5Y',
        '2.5Y' : '2Y',
        '3Y' : '2.5Y',
        '3.5Y' : '3Y',
        '4Y' : '3.5Y',
        '4.5Y' : '4Y',
        '5Y' : '4.5Y',
        '5.5Y' : '5Y',
        '6' : '5.5',
        '6.5' : '6',
        '7' : '6',
        '7.5' : '6.5',
        '8' : '7',
        '8.5' : '7.5',
        '9' : '8',
        '9.5' : '8.5',
        '10' : '9',
        '10.5' : '9.5',
        '11' : '10',
        '11.5' : '10.5',
        '12' : '11',
        '12.5' : '11.5',
        '13' : '12',
        '13.5' : '12.5',
        '14' : '13',
        '15' : '14'
    }
    _cm2uk = {
        '116' : 'Junior XS',
        '128' : 'Junior S',
        '140' : 'Junior M',
        '152' : 'Junior L',
        '164' : 'Junior XL',
        '176' : 'Junior XXL'
    }

    @classmethod
    def csv(cls):
        return ((Suppress('"') + Word(alphanums + '-/.,') + Suppress('"'))|(Word(alphanums + "-/.")))

    @classmethod
    def adidas_code(cls, s, l, t):
        def _adidas_code(s,l,t):
            return {'manufacturer_code':t[0], 'color_code':t[0]}

        return cls.csv().setParseAction(_adidas_code)

    @classmethod
    def nike_code(cls, s, l, t):
        def _nike_code(s,l,t):
            return {'manufacturer_code':t[0]}

        return cls.csv().setParseAction(_nike_code)

    @classmethod
    def reusch_code(cls, s, l, t):
        def _reusch_code(s,l,t):
            return {'manufacturer_code':t[0]}

        return cls.csv().setParseAction(_reusch_code)

    @classmethod
    def uhlsport_code(cls, s, l, t):
        def _uhlsport_code(s,l,t):
            return {'manufacturer_code':t[0], 'color_code':t[0][7:9]}

        return cls.csv().setParseAction(_uhlsport_code)

    @classmethod
    def puma_code(cls, s, l, t):
        def _puma_code(s,l,t):
            return {'manufacturer_code':t[0], 'color_code':t[0][-2:]}

        return cls.csv().setParseAction(_puma_code)

    @classmethod
    def nike_color(cls, s, l, t):
        def _nike_color(s,l,t):
            return {'color_code':t[0].zfill(3)}

        return cls.csv().setParseAction(_nike_color)

    @classmethod
    def reusch_color(cls, s, l, t):
        def _reusch_color(s,l,t):
            return {'color_code':t[0]}

        return cls.csv().setParseAction(_reusch_color)

    @classmethod
    def size(cls, s, l, t):
        def _size(s,l,t):
            value = t[0].replace("-", ".5").replace(",", ".").replace("2XL","XXL").replace("NOSIZE","one-size")

            return {'size_code': [value]}

        return cls.csv().setParseAction(_size)

    @classmethod
    def adidas_size(cls, s, l, t):
        def _adidas_size(s,l,t):
            value = t[0].replace("-", ".5").replace(",", ".").replace("2XL","XXL").replace("NOSIZE","one-size")

            if value.isdigit() and len(value) == 4:
                value = f'{value[:2]}-{value[-2:]}'

            values = [value]

            return {'size_code': values}

        return cls.csv().setParseAction(_adidas_size)

    @classmethod
    def adidas_size_shoes(cls, s, l, t):
        def _adidas_size_shoes(s,l,t):
            value = t[0].replace("-", ".5")

            if value in ['28','28.5','29','29.5','30','30.5','31','31.5','32','32.5','33','33.5','34','34.5','35']:
                value = value.replace('.5',' 1/2')
            if value in ['3','3.5','4','4.5','5','5.5']:
                value += 'Y'

            values = [value]

            return {'size_code': values}

        return cls.csv().setParseAction(_adidas_size_shoes)

    @classmethod
    def nike_size(cls, s, l, t):
        def _nike_size(s,l,t):
            value = t[0].replace("-", ".5").replace(",", ".").replace("2XL","XXL").replace("NOSIZE","one-size")
            values = [value]

            if value in ['XS','S','M','L','XL','XXL']:
                values.append(f'Junior {value}')

            return {'size_code': values}

        return cls.csv().setParseAction(_nike_size)

    @classmethod
    def nike_size_shoes(cls, s, l, t):
        def _nike_size_shoes(s,l,t):
            value = t[0].replace(',','.').replace("-", ".5")
            value = cls._us2uk.get(value)
            values = [value]

            return {'size_code': values}

        return cls.csv().setParseAction(_nike_size_shoes)

    @classmethod
    def puma_size(cls, s, l, t):
        def _puma_size(s,l,t):
            value = t[0].replace("-", ".5").replace(",", ".").replace("2XL","XXL").replace("NOSIZE","one-size")
            value = 'one-size' if value == 'X' else value
            values = [value]

            if cls._cm2uk.get(value):
                values.append(cls._cm2uk.get(value))

            return {'size_code': values}

        return cls.csv().setParseAction(_puma_size)

    @classmethod
    def puma_size_shoes(cls, s, l, t):
        def _puma_size_shoes(s,l,t):
            value = t[0].replace("-", ".5").replace(",", ".").replace("2XL","XXL").replace("NOSIZE","one-size")
            values = [value]

            if value in ['1','1.5','2','2.5','3','3.5','4','4.5','5','5.5']:
                values.append(f'{value}Y')
            if value in ['10','10.5','11','11.5','12','12.5','13','13.5']:
                values.append(f'{value}K')

            return {'size_code': values}

        return cls.csv().setParseAction(_puma_size_shoes)

    @classmethod
    def ean(cls, s, l, t):
        def _ean(s,l,t):
            return {'ean':t[0].zfill(12)}

        return cls.csv().setParseAction(_ean)

    @classmethod
    def price_cz(cls, s, l, t):
        def _price_cz(s,l,t):
            return {'price_cz': float(t[0])}

        return cls.csv().setParseAction(_price_cz)

    @classmethod
    def price_sk(cls, s, l, t):
        def _price_sk(s,l,t):
            return {'price_sk': float(t[0])}

        return cls.csv().setParseAction(_price_sk)

    @classmethod
    def style_code(cls, s, l, t):
        def _style_code(s,l,t):
            return {'manufacturer_code':t[0]}

        return cls.csv().setParseAction(_style_code)

class Ean:
    def __init__(self):
        self.db = Db()
        self.filters = Filters()

    def OR(self,list):
        OR = NoMatch()
        for l in list:
            OR |= l
        return OR

    def AND(self,list):
        OR = Empty()
        for l in list:
            OR += l
        return OR

    def get_csvs(self,path):
        with os.scandir(path) as files:
            for file in files:
                if file.name.endswith(".csv") and not os.path.isfile(file.path.replace('input','output').replace('.csv','.txt')):
                    yield file


    def parse_csv(self,f_input):
        header = f_input.readline()

        try:
            import_ean = []
            import_ean.append(Keyword("adidas_code").setParseAction(Filters.adidas_code))
            import_ean.append(Keyword("nike_code").setParseAction(Filters.nike_code))
            import_ean.append(Keyword("reusch_code").setParseAction(Filters.reusch_code))
            import_ean.append(Keyword("uhlsport_code").setParseAction(Filters.uhlsport_code))
            import_ean.append(Keyword("puma_code").setParseAction(Filters.puma_code))
            import_ean.append(Keyword("nike_color").setParseAction(Filters.nike_color))
            import_ean.append(Keyword("reusch_color").setParseAction(Filters.reusch_color))
            import_ean.append(Keyword("size").setParseAction(Filters.size))
            import_ean.append(Keyword("adidas_size").setParseAction(Filters.adidas_size))
            import_ean.append(Keyword("nike_size").setParseAction(Filters.nike_size))
            import_ean.append(Keyword("puma_size").setParseAction(Filters.puma_size))
            import_ean.append(Keyword("puma_size_shoes").setParseAction(Filters.puma_size_shoes))
            import_ean.append(Keyword("adidas_size_shoes").setParseAction(Filters.adidas_size_shoes))
            import_ean.append(Keyword("nike_size_shoes").setParseAction(Filters.nike_size_shoes))
            import_ean.append(Keyword("ean").setParseAction(Filters.ean))

            import_ean = (Suppress(LineStart()) + OneOrMore(self.OR(import_ean) + Optional(",").setParseAction(lambda s, l, t: Optional(Suppress(",")))) + Suppress(LineEnd())).parseString(header)
            import_ean = self.AND(import_ean)
        except ParseException:
            import_ean = NoMatch()
        finally:
            import_ean = (Suppress(LineStart()) + import_ean + Suppress(LineEnd()))


        try:
            import_price = []
            import_price.append(Keyword("price_cz").setParseAction(Filters.price_cz))
            import_price.append(Keyword("price_sk").setParseAction(Filters.price_sk))
            import_price.append(Keyword("ean").setParseAction(Filters.ean))
            import_price.append(Keyword("style_code").setParseAction(Filters.style_code))

            import_price = (Suppress(LineStart()) + OneOrMore(self.OR(import_price) + Optional(",").setParseAction(lambda s, l, t: Optional(Suppress(",")))) + Suppress(LineEnd())).parseString(header)
            import_price = self.AND(import_price)
        except ParseException:
            import_price = NoMatch()
        finally:
            import_price = (Suppress(LineStart()) + import_price + Suppress(LineEnd()))


        for i, line in enumerate(f_input):
            try:
                values = import_ean.parseString(line)
                values = {k:v for d in values for k,v in d.items()}
                status_ean = self.import_ean(values)
            except ParseException:
                status_ean = 'WRONG FORMAT'

            try:
                values = import_price.parseString(line)
                values = {k:v for d in values for k,v in d.items()}
                status_price = self.import_price(values)
            except ParseException:
                status_price = 'WRONG FORMAT'

            yield ','.join([status_ean, status_price, line])


    def import_ean(self,values):
        state = None
        results = []
        for size in values['size_code']:
            result = self.db.getByGeneratedEan(manufacturer_code=values['manufacturer_code'],color_code=values['color_code'],size_code=size).fetchall()
            results += result

        if len(results) == 1:
            result = results[0]

            if not values['ean']:
                state = 'MISSING EAN'
            elif not result['ean'] and values['ean']:
                #self.db.putEan(result['generated_ean'],values['ean'])
                state = f'{len(results)}x UPDATED'
            elif result['ean'] == values['ean']:
                state = 'AS IS'
            elif result['ean'] != values['ean']:
                state = 'EAN IN COLISION'
            else:
                state = 'UNKNOWN STATE'
        elif len(results) > 1:
            state = 'MORE THAN ONE MATCH'
        else:
            state = 'NO MATCH'

        return state

    def import_price(self,values):

        print(values);

        price_cz = values.pop('price_cz', 0)
        price_sk = values.pop('price_sk', 0)

        results = self.db.getByEan(**values).fetchall()

        if not price_cz and not price_sk:
            state = 'MISSING PRICE'
        elif len(results) > 0:
            for result in results:
                id = result['id']
                self.db.putPrice(id,price_cz,price_sk)
            state = f'{len(results)}x UPDATED'
        else:
            state = 'NO MATCH'

        return state

def main():
    ean = Ean()
    d = os.path.join(os.path.dirname(__file__),'input')
    for f_csv in ean.get_csvs(d):
        print(f_csv.path)
        with open(f_csv.path, 'r', errors='ignore') as f_input, open(f_csv.path.replace('input','output').replace('.csv','.txt'), 'w') as f_output:
            f_output.write(','.join(['status_ean', 'status_price', 'line\n']))
            for i,line in enumerate(ean.parse_csv(f_input)):
                f_output.write(line)

if __name__ == '__main__':
    main()
