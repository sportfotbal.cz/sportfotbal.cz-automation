sportfotbal.cz-automation
======================

This repository contains set of scripts for automation to be used with `sportfotbalcz`_ private ecommerce solution.

- ``sitemap.py`` - export categories, products and texts
- ``ean.py`` - import product EAN codes from csv

sitemap.py
----------
Export categories, products and texts. Connects database directly and pull data directly from database. Command doesn't take any arguments and has no configuration options.

.. code-block:: bash

    python sitemap.py

ean.py
------
Import product EAN codes from csv. Connects database and push data directly to database. Command doesn't take any arguments,
instead first column of csv (header) is argument determining what data are in relevant column. List of possible columns will be listed shortly.
Input file needs to be located in relative folder ``input`` and file with output will be created and located in relative folder ``output``.

.. code-block:: bash

    python ean.py
..

**List of possible options**:

 |
 | Currently there is not implemented any kind of validation, except simple check if there are all necessary and valid header options
 |

===================== ===================================
**General:**
``ean``               as it is
|
**Product code:**
``adidas_code``       it's code and it's color
``nike_code``         as it is
``reusch_code``       as it is
``uhlsport_code``     it's code and 8th and 9th character is color
``puma_code``         it's code and last two characters is color
|
**Product color:**
``nike_color``        3 characters, if needed leading zeros are added
``reusch_color``      as it is
|
**Product size:**
``size``              replace('-','.5'), replace('2XL','XXL'), replace('NOSIZE','one-size') and when string is digit 4 character long, '-' is inserted in the middle
``adidas_size_shoes`` '28 - 35' : replace('.5','1/2') and behind '3 - 5.5' character 'Y' is added
``nike_size_shoes``   replace('-','.5')
===================== ===================================

Deployment
------------
Because it's advisable to run scripts  in isolated environment, here is basic guideline with use of `venv`_ (python3) and helper files ``ean.sh``, ``sitemap.sh``

.. code-block:: bash

    python3 -m venv .venv
    sh ean.sh
    sh sitemap.sh
..

Links
------------
- `venv`_
- `sportfotbalcz`_

.. _venv: https://docs.python.org/3/library/venv.html
.. _sportfotbalcz: https://www.sportfotbal.cz/
