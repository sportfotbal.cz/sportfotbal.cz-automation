#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
Import EANs from csv files.

Import EANs from csv files and run each column through filter by header column definition
"""
import sys
import os
import csv
import functools
import pymysql
import configparser
import time
import xml.etree.cElementTree as cElementTree
from pyparsing import Word, alphas, OneOrMore, Suppress, ZeroOrMore, LineStart, LineEnd, oneOf, Optional, alphanums, Keyword, Group, Empty, NoMatch,  nums, ParseException
from xml.dom import minidom


CONFIG = configparser.ConfigParser()
CONFIG.read(os.path.join(os.path.dirname(__file__),'config.ini'))

class Pymysql:
    def __init__(self, id_domain = 1, id_lang = 1, media_url = 'https://www.sportfotbal.cz/data'):
        self.connection = pymysql.connect(host=CONFIG['ean']['host'],
                                     user=CONFIG['ean']['user'],
                                     password=CONFIG['ean']['password'],
                                     db=CONFIG['ean']['db'],
                                     charset=CONFIG['ean']['charset'],
                                     cursorclass=pymysql.cursors.DictCursor)
        self.id_domain = id_domain
        self.id_lang = id_lang
        self.media_url = media_url

    def query(self, query, data=()):
        with self.connection.cursor() as cursor:
            cursor.execute(query, data)
            return cursor

    def get_domain(self):
        cursor = self.query('SELECT * from domain WHERE id = %s',(self.id_domain))
        return cursor

    def get_products(self):
        cursor = self.query('SELECT *,image.url as image_url from product INNER JOIN product_visible ON product_visible.id_product = product.id INNER JOIN product_lang ON product_lang.id_product = product.id INNER JOIN image ON image.id_product = product.id WHERE product_visible.id_domain = %s AND product_lang.id_lang = %s AND product_visible.visible >= 1 AND product.deleted = 0 AND image.head = 1',(self.id_domain, self.id_lang))
        return cursor

    def get_texts(self):
        cursor = self.query('SELECT * from text INNER JOIN text_lang ON text_lang.id_text = text.id WHERE id_domain = %s AND id_lang = %s AND no_index = 1',(self.id_domain, self.id_lang))
        return cursor

    def get_systems(self):
        cursor = self.query('SELECT * from text_system INNER JOIN text_system_lang ON text_system_lang.id_text = text_system.id WHERE id_domain = %s AND id_lang = %s AND no_index = 1',(self.id_domain, self.id_lang))
        return cursor

    def get_categories(self):
        cursor = self.query('SELECT category.id,category.id_type, category_lang.rewrite_title, category_lang.filter_url, category.updated from category INNER JOIN category_domain ON category_domain.id_category = category.id INNER JOIN category_lang ON category_lang.id_category = category.id WHERE id_domain = %s AND id_lang = %s AND visible = %s',(self.id_domain, self.id_lang, 1))
        return cursor

class SitemapGenerator:
    def __init__(self):
        self.root = cElementTree.Element('urlset')
        self.root.attrib['xmlns']="http://www.sitemaps.org/schemas/sitemap/0.9"
        self.root.attrib['xmlns:image']="http://www.google.com/schemas/sitemap-image/1.1"

    def add_item(self,loc,lastmod,image = None):
        element_url = cElementTree.SubElement(self.root, "url")
        cElementTree.SubElement(element_url, "loc").text = loc
        cElementTree.SubElement(element_url, "lastmod").text = str(lastmod.isoformat())

        if image:
            element_image = cElementTree.SubElement(element_url, "image:image")
            cElementTree.SubElement(element_image, "image:loc").text = image['url']
            cElementTree.SubElement(element_image, "image:title").text = image['title']
            cElementTree.SubElement(element_image, "image:caption").text = image['title']


    def write(self,file):
        xmlstr = minidom.parseString(cElementTree.tostring(self.root)).toprettyxml(indent="   ")
        with open(file, "w") as f:
            f.write(xmlstr)

def main():
    for id_domain in [1,7,9,10,11]:
        db = Pymysql()

        db.id_domain = id_domain
        domain = db.get_domain().fetchone()
        db.id_lang = domain['id_pref_lang']
        db.media_url = domain['media_url']

        sitemap_products = SitemapGenerator()
        sitemap_categories = SitemapGenerator()
        sitemap_texts = SitemapGenerator()

        for p in db.get_products():
            sitemap_products.add_item(f'{domain["url"]}/{p["rewrite_title"]}',p['updated'],{'url' : f'{db.media_url}/products/{p["image_url"]}', 'title' : p['title']})

        for c in db.get_categories():
            url = c['rewrite_title'] if c['id_type'] != 2 else c['filter_url']
            sitemap_categories.add_item(f'{domain["url"].rstrip("/")}/{url.strip("/")}/',c['updated'])

        for t in db.get_texts():
            url = t['rewrite_title']
            sitemap_texts.add_item(f'{domain["url"].rstrip("/")}/{url.lstrip("/")}.html',t['updated'])
        for s in db.get_systems():
            url = s['rewrite_title']
            sitemap_texts.add_item(f'{domain["url"].rstrip("/")}/{url.lstrip("/")}.html',s['updated'])

        sitemap_products.write(os.path.join(os.path.dirname(__file__),'sitemap',f'sitemap_products_{id_domain}.xml'))
        sitemap_categories.write(os.path.join(os.path.dirname(__file__),'sitemap',f'sitemap_categories_{id_domain}.xml'))
        sitemap_texts.write(os.path.join(os.path.dirname(__file__),'sitemap',f'sitemap_texts_{id_domain}.xml'))

if __name__ == '__main__':
    main()



#distinc
